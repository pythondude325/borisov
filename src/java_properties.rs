use anyhow::Result;
use std::collections;
use std::fmt::Write;
use tokio::io::{self, AsyncWriteExt};

pub async fn write_properties_file<W: io::AsyncWrite + Unpin>(
    writer: &mut W,
    entries: &collections::HashMap<String, String>,
    comment: Option<&str>,
) -> Result<()> {
    let mut buffer = String::new();

    if let Some(comment) = comment {
        writeln!(&mut buffer, "#{}", comment)?;
    }
    // The time zone name is wrong, just print the offset for now.
    // https://github.com/chronotope/chrono/issues/288
    writeln!(
        &mut buffer,
        "#{}",
        chrono::Local::now().format("%a %b %d %T %Z %Y")
    )?;
    for (key, value) in entries {
        // This should properly escape the keys and values.
        // https://docs.oracle.com/javase/7/docs/api/java/util/Properties.html#store(java.io.Writer,%20java.lang.String)
        writeln!(&mut buffer, "{}={}", key, value)?;
    }

    writer.write_all(buffer.as_bytes()).await?;

    Ok(())
}
