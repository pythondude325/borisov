use anyhow::{anyhow, Context, Result};
use futures::stream::TryStreamExt;
use std::path;
use tokio::{fs, io::AsyncWriteExt};

async fn get_download_url(version: &str) -> Result<String> {
    let url = format!("https://mcversions.net/download/{}", version)
        .parse::<reqwest::Url>()
        .context("Unable to parse URL")?;

    let response = reqwest::get(url)
        .await
        .context("Unable to get download index")?;

    if response.status() == 404 {
        return Err(anyhow!("Version not found"));
    }

    let html = response.text().await?;

    let document = select::document::Document::from(html.as_ref());

    document
        .find(select::predicate::Attr("download", ()))
        .find_map(|node| match node.attr("href") {
            Some(url) if url.ends_with("server.jar") => Some(url.to_string()),
            _ => None,
        })
        .ok_or_else(|| anyhow!("No download found"))
}

pub async fn download_jar(version: String, jar_path: path::PathBuf) -> Result<()> {
    let download_url: reqwest::Url = get_download_url(&version).await?.parse()?;

    let mut jar_file = fs::File::create(&jar_path).await?;
    let jar_response = reqwest::get(download_url).await?;
    if jar_response.status() == 404 {
        return Err(anyhow!("JAR not found"));
    }
    let mut jar_stream = jar_response.bytes_stream();

    while let Some(bytes) = jar_stream.try_next().await? {
        jar_file.write_all(&bytes).await?;
    }

    Ok(())
}
